﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ControlSettingsMap
{
    public class SettingEditor : PropertyEditor
    {
        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            { nameof(ClassSettings.FormCoord), "TypeViewCoordEditor" },
            {nameof(ClassSettings.IP_DB),"IpEditor" },
            {nameof(ClassSettings.NumPort), "PortEditor" },
            { nameof(ClassSettings.Path), "PathEditor"},
            { nameof(ClassSettings.PathMatr), "PathEditor"},
            {nameof(ClassSettings.SysCoord),"TypeSystCoordEditor" },
            {nameof(ClassSettings.TypeProjection), "TypeProjectionEditor" }
        };

        public SettingEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ControlSettingsMap;component/Resources/Dictionary.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }
    }
}
