﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace ControlSettingsMap
{
    public enum SystemCoordinates : byte
    {
        WGS84 = 0,
        SK42_degree = 1,
        SK42_XY = 2,
        Mercator_XY = 3
    }

    public enum FormatCoordinates : byte
    {
        [Description("DD.dddddd")]
        DecimalDegree = 0,
        [Description("DD MM.mmmm")]
        DegreeMinSec = 1,
        [Description("DD MM SS.ss")]
        DegreeDecMin = 2
    }

    public enum TypesProjection : byte
    {
        Mercator,
        Geografic
    }

    public enum Languages
    {
        [Description("Русский")]
        Rus,
        [Description("English")]
        Eng,
        [Description("Azərbaycan")]
        Az
    }

    [CategoryOrder(CategoryDB,1)]
    [CategoryOrder(CategoryCoordSys, 2)]
    [CategoryOrder(CategoryCoordFormat, 3)]
    [CategoryOrder(CategoryCommon, 4)]
    public class ClassSettings : INotifyPropertyChanged
    {
        #region Public

        public const string CategoryDB = "DataBase";
        public const string CategoryCoordSys = "CoordSystem";
        public const string CategoryCoordFormat = "CoordFormat";
        public const string CategoryCommon = "Common";

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string ipDb = "";
        private int numPort = 0;
        private SystemCoordinates sysCoord;
        private FormatCoordinates formCoord;
        private string pathMap = "";
        private string pathMatrix = "";
        private double scale;
        private TypesProjection typeProjection;
        private Languages language;

        #endregion

        #region Properties

        [DataMember]
        [Category(CategoryDB)]
        [Browsable(true)]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IP_DB
        {
            get=>ipDb;
            set
            {
                if (ipDb == value) return;
                ipDb = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(CategoryDB)]
        [Browsable(true)]
        [Required]
        public int NumPort
        {
            get => numPort;
            set
            {
                if (numPort == value) return;
                numPort = value;
                OnPropertyChanged();
            }
        }

        [Category(CategoryCoordSys)]
        [Browsable(true)]
        public SystemCoordinates SysCoord
        {
            get => sysCoord;
            set
            {
                if (sysCoord == value) return;
                sysCoord = value;
                OnPropertyChanged();
            }
        }

        [Category(CategoryCoordFormat)]
        [Browsable(true)]
        [Required]
        public FormatCoordinates FormCoord
        {
            get => formCoord;
            set
            {
                if (formCoord == value) return;
                formCoord = value;
                OnPropertyChanged();
            }
        }
               
        // Path to map
        [Category(CategoryCommon)]
        [Browsable(true)]
        public string Path
        {
            get => pathMap;
            set
            {
                if (pathMap == value) return;
                pathMap = value;
                OnPropertyChanged();
            }
        }

        // Path to height matrix
        [Category(CategoryCommon)]
        [Browsable(true)]
        public string PathMatr
        {
            get => pathMatrix;
            set
            {
                if (pathMatrix == value) return;
                pathMatrix = value;
                OnPropertyChanged();
            }
        }

        [Category(CategoryCommon)]
        public TypesProjection TypeProjection
        {
            get => typeProjection;
            set
            {
                if (typeProjection == value) return;
                typeProjection = value;
                OnPropertyChanged();
            }
        }

        [Category(CategoryCommon)]
        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double Scale
        {
            get => scale;
            set
            {
                if (scale == value) return;
                scale = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Methods

        public ClassSettings Clone()
        {
            return new ClassSettings
            {
                FormCoord = formCoord,
                IP_DB = ipDb,
                NumPort = numPort,
                SysCoord = sysCoord,
                Path = pathMap,
                PathMatr = pathMatrix,
                Scale = scale,
                TypeProjection = typeProjection,
                Language = Language
            };
        }

        public void Update(ClassSettings newSettings)
        {
            FormCoord = newSettings.FormCoord;
            IP_DB = newSettings.IP_DB;
            NumPort = newSettings.NumPort;
            SysCoord = newSettings.SysCoord;
            Path = newSettings.Path;
            PathMatr = newSettings.PathMatr;
            Scale = newSettings.Scale;
            TypeProjection = newSettings.TypeProjection;
            Language = newSettings.Language;
        }

        public bool Compare(ClassSettings classSettings)
        {
            if (classSettings.SysCoord != SysCoord || 
                classSettings.NumPort != NumPort || 
                classSettings.IP_DB != IP_DB || 
                classSettings.FormCoord != FormCoord ||
                classSettings.PathMatr != PathMatr ||
                classSettings.Path != Path ||
                classSettings.TypeProjection != TypeProjection ||
                classSettings.Language != Language)
                return false;
            return true;
        }

        #endregion

    }
} 
