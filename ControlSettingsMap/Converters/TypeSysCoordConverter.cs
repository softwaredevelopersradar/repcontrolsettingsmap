﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ControlSettingsMap
{
    public class TypeSysCoordConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (SystemCoordinates)System.Convert.ToByte(value);
        }
    }
}
