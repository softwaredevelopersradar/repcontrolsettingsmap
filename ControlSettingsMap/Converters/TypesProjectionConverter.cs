﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ControlSettingsMap
{
    public class TypesProjectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (TypesProjection)System.Convert.ToByte(value);
        }
    }
}
