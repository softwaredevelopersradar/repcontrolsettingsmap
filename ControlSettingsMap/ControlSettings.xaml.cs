﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace ControlSettingsMap
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlSettings : UserControl
    {
        private ClassSettings oldSettings;

        private Dictionary<string, string> TranslateDic;

        private string pathResourceEng = "/ControlSettingsMap;component/Languages/StringResource.EN.xaml";
        private string pathResourceAz = "/ControlSettingsMap;component/Languages/StringResource.Az.xaml";
        private string pathResourceRus = "/ControlSettingsMap;component/Languages/StringResource.xaml";

        public ControlSettings()
        {
            InitializeComponent();
            InitEditor();
            InitComponent();
            ChangeLanguge();
            Settings.PropertyChanged += Settings_PropertyChanged;
        }


        private void InitEditor()
        {
            //PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.FormCoord), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.IP_DB), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.NumPort), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.Path), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.PathMatr), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.SysCoord), Settings.GetType()));
            PropertyMap.Editors.Add(new SettingEditor(nameof(Settings.TypeProjection), Settings.GetType()));
        }

        private void InitComponent()
        {
            oldSettings = Settings.Clone();
        }

        //private void TranslateCategory()
        //{
        //    switch (CurrentLanguage)
        //    {
        //        case Languages.Rus:
        //            PropertyMap.Categories.FirstOrDefault(t => t.Name == ClassSettings.CategoryDB).HeaderCategoryName = "База данных";
        //            PropertyMap.Categories.FirstOrDefault(t => t.Name == ClassSettings.CategoryCoordSys).HeaderCategoryName = "Система координат";
        //            PropertyMap.Categories.FirstOrDefault(t => t.Name == ClassSettings.CategoryCoordFormat).HeaderCategoryName = "Формат координат";
        //            PropertyMap.Categories.FirstOrDefault(t => t.Name == ClassSettings.CategoryCommon).HeaderCategoryName = "Общие";
        //            break;
        //        case Languages.Eng:
        //            break;
        //        default:
        //            break;
        //    }
        //}


        #region Language

        private void ChangeLanguge()
        {
            try
            {
                LoadDictionary(Settings.Language);
                SetResourcelanguage(Settings.Language);
                UpdateNameCategory();
                UpdateNameProperty();
            }
            catch (Exception)
            {

            }
        }

        private void UpdateNameCategory()
        {
            try
            {
                foreach (var category in PropertyMap.Categories)
                {
                    if (TranslateDic.ContainsKey(category.Name))
                        PropertyMap.Categories.First(t => t.Name == category.Name).HeaderCategoryName = TranslateDic[category.Name];
                }
            }
            catch (Exception)
            {

            }
        }

        private void UpdateNameProperty()
        {
            try
            {
                foreach (var property in PropertyMap.Properties)
                {
                    if (TranslateDic.ContainsKey(property.Name))
                    {
                        PropertyMap.Properties.First(t => t.Name == property.Name).DisplayName = TranslateDic[property.Name];
                        PropertyMap.Properties[property.Name].ToolTip = TranslateDic[property.Name];
                    } 
                }
            }
            catch (Exception)
            {

            }
        }


        private void SetResourcelanguage(Languages lang)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (lang)
                {
                    case Languages.Eng:
                        dict.Source = new Uri(pathResourceEng, UriKind.Relative);
                        break;
                    case Languages.Rus:
                        dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        break;
                    case Languages.Az:
                        dict.Source = new Uri(pathResourceAz, UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        void LoadDictionary(Languages lang)
        {
            var translation = Properties.Resources.TranslationSettingsMap;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Settings.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }

        #endregion

        #region Handler

        private void Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Settings.Language))
            {
                OnLanguageChanged(this, Settings.Language);
                ChangeLanguge();
            }
        }
        #endregion

        #region Event

        public event EventHandler<ClassSettings> OnUpdateSettings = (object sender, ClassSettings eventArgs) => { };

        public event EventHandler<Languages> OnLanguageChanged  = (sender, language) => { };

        #endregion

        #region Public


        public ClassSettings Settings
        {
            get
            {
                return (ClassSettings)Resources["SettingsMap"];
            }
            set
            {

                if ((ClassSettings)Resources["SettingsMap"] == value)
                {
                    oldSettings = value.Clone();
                    return;
                }
                ((ClassSettings)Resources["SettingsMap"]).Update(value);
                oldSettings = Settings.Clone();
            }
        }

        #endregion
                     
        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (!Settings.Compare(oldSettings))
            {
                oldSettings = Settings.Clone();
                OnUpdateSettings(this, Settings);
            }
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (!Settings.Compare(oldSettings))
                Settings.Update(oldSettings);
        }
    }
}
